<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sp_f" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>
	<head>
		<title>SPRING SECURITY</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
		    <div class="panel">
		       <div class="panel-heading">
		          <h2>Register new account</h2>
		       </div>
		       <div class="panel-body">
			          <sp_f:form method="POST" modelAttribute="user" class="form">
			              <spring:bind path="username">
			                 <sp_f:input type="text" path="username" class="form-control" placeholder="Enter your username here"></sp_f:input>
			                 <sp_f:errors path="username"></sp_f:errors>
			              </spring:bind>
			              <spring:bind path="password">
			                 <sp_f:input type="password" class="form-control" path="password" placeholder="Enter your password here"></sp_f:input>
			                 <sp_f:errors path="password"></sp_f:errors>
			              </spring:bind>
			              
			              <button type="submit" class="btn btn-lg btn-block btn-primary">Create</button>
			          </sp_f:form>
		       </div>
		    </div>
		</div>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>	
	</body>
</html>