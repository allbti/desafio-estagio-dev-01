package com.alexandrelopes.son_security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alexandrelopes.son_security.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
